# k8s Prometheus MySQL Exporter

Forked from https://github.com/prometheus/mysqld_exporter

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Setting up MySQL, see
https://dev.mysql.com/doc/refman/5.7/en/innodb-information-schema-metrics-table.html

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name      |   Description    | Default     |
| :-------------------- | ---------------- | ----------- |
|  `DATA_SOURCE_USER`   | MySQL Username   | undef       |
|  `DATA_SOURCE_PASS`   | MySQL Password   | undef       |
|  `DATA_SOURCE_HOST`   | MySQL Host       | `127.0.0.1` |
|  `DATA_SOURCE_PORT`   | MySQL Port       | `3306`      |
