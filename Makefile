SKIP_SQUASH?=1
IMAGE=opsperator/mysqlexporter
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: clean
clean:
	@@docker rm -f testmysql || true
	@@docker rm -f testmyxp || true

.PHONY: testmy
testmy:
	@@docker rm -f testmysql || true
	@@docker run \
	    -e MYSQL_ROOT_PASSWORD=secret \
	    -e MYSQL_DATABASE=testdb \
	    -e MYSQL_USER=myusr \
	    -e MYSQL_PASSWORD=testpw \
	    -p 27017:27017 \
	    --name testmysql \
	    -d docker.io/centos/mariadb-102-centos7:latest

.PHONY: testmyok
testmyok:
	if ! docker exec testmysql /bin/sh -c 'echo SHOW TABLES | mysql -u root $$MYSQL_DATABASE && echo OK' | grep OK; then \
	    echo MySQL KO; \
	    exit 1; \
	fi; \
	echo MySQL OK

.PHONY: test
test:
	@@if ! make testmyok; then \
	    make testmy; \
	    sleep 120; \
	fi
	@@docker rm -f testmyxp || true
	@@myip=`docker inspect testmysql | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	docker run --name testmyxp \
	    -e DEBUG=yay \
	    -p 9113:9113 \
	    -e DATA_SOURCE_HOST=$$myip \
	    -e DATA_SOURCE_USER=root \
	    -e DATA_SOURCE_PASS=secret \
	    -it $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secret service deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "MYSQL_EXPORTER_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "MYSQL_EXPORTER_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
