FROM docker.io/golang:1.14 AS builder

# MySQL Exporter image for OpenShift Origin

WORKDIR /go/src/github.com/prometheus/mysqld_exporter

COPY config/collector ./collector
COPY config/mysqld-mixin ./mysqld-mixin
COPY config/mysqld_exporter.go config/go.* ./

RUN set -x \
    && if test `uname -m` = aarch64; then \
	export GOARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export GOARCH=arm; \
    else \
	export GOARCH=amd64; \
    fi \
    && env GO15VENDOREXPERIMENT=1 \
	CGO_ENABLED=0 \
	GOOS=linux \
	go get ./ \
    && env GO15VENDOREXPERIMENT=1 \
	CGO_ENABLED=0 \
	GOOS=linux \
	go build -o mysqld_exporter mysqld_exporter.go

FROM scratch

LABEL io.k8s.description="MySQL Prometheus Exporter Image." \
      io.k8s.display-name="MySQL Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,mysql" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-mysqlexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="0.13.0"

COPY --from=builder \
	/go/src/github.com/prometheus/mysqld_exporter/mysqld_exporter \
	/mysqld_exporter

ENTRYPOINT ["/mysqld_exporter"]
USER 1001
